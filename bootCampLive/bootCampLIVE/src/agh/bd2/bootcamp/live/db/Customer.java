package agh.bd2.bootcamp.live.db;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

public class Customer {
	private long id;

	private String name;

	//private CustomerDetails details;

	//private List<Order> orders = new ArrayList<Order>(); 

	public Customer(String name) {
		super();
		this.name = name;

	}
	// chyba czegos tu brakuje

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	
//	 public CustomerDetails getDetails() {
//	 return details;
//	 }
//	
//	 public void setDetails(CustomerDetails details) {
//	 this.details = details;
//	 }
//	
//	public List<Order> getOrders() {
//		return orders;
//	}
//	public Customer addOrder(Order order){
//		order.setBuyer(this);
//		this.orders.add(order);
//		return this;
//	}
}
