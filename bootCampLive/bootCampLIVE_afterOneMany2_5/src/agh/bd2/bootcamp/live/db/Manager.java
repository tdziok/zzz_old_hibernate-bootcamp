package agh.bd2.bootcamp.live.db;

public class Manager extends Employee {

	private String position;

	public Manager(String name, String surname, String position) {
		super(name, surname);
		this.position = position;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
	
}
