package agh.bd2.bootcamp.live.db;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class Customer {

	@Id
	@GeneratedValue
	@Column(name = "CUSTOMER_ID")
	private long id;

	@Column(length = 255)
	private String name;

	@OneToOne(cascade=CascadeType.ALL)
	private CustomerDetails details;
	
	@OneToMany(mappedBy="buyer",cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	private List<Order> orders = new ArrayList<Order>(); 

	public Customer(String name) {
		super();
		this.name = name;

	}

	Customer() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	
	 public CustomerDetails getDetails() {
	 return details;
	 }
	
	 public void setDetails(CustomerDetails details) {
	 this.details = details;
	 }
	
	public List<Order> getOrders() {
		return orders;
	}
	public Customer addOrder(Order order){
		order.setBuyer(this);
		this.orders.add(order);
		return this;
	}
}
