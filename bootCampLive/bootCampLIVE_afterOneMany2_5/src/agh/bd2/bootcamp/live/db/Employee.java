package agh.bd2.bootcamp.live.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Employee {
	@Id
	@GeneratedValue
	private long id;
	@Column(length=255)
	private String name;
	@Column(length=255)
	private String surname;

	public Employee(String name, String surname) {
		super();
		this.name = name;
		this.surname = surname;
	}
	Employee() {
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	
	
}
