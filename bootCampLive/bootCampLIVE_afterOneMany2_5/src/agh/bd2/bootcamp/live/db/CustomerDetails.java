package agh.bd2.bootcamp.live.db;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class CustomerDetails {

	@Id
	@GeneratedValue
	private long id;
	
	@Column(length=255)
	private String address;

	@Column(length=255)
	private String phone;

	
	private int rank;

	@OneToOne(cascade = CascadeType.ALL, mappedBy="details")
	@PrimaryKeyJoinColumn
	private Customer customer;

	public CustomerDetails(String address, String phone, int rank) {
		super();
		this.address = address;
		this.phone = phone;
		this.rank = rank;
	}

	CustomerDetails() {
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}
