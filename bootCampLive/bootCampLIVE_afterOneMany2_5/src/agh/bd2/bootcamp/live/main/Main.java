package agh.bd2.bootcamp.live.main;

import java.math.BigDecimal;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import agh.bd2.bootcamp.live.db.Customer;
import agh.bd2.bootcamp.live.db.CustomerDetails;
import agh.bd2.bootcamp.live.db.Employee;
import agh.bd2.bootcamp.live.db.Order;

public class Main {

	public static void main(String[] args) {
		SessionFactory sessionFactory = HibernateUtils.getSessionfactory();

		// tworzymy sesjse
		Session session = sessionFactory.openSession();
		// zaczynamy transakcje
		session.beginTransaction();

		// zapisujemy obiekty

		Customer customer1 = new Customer("HP");
		CustomerDetails customerDetails = new CustomerDetails("Krakowska",
				"111111111", 10);

		customer1.setDetails(customerDetails);
		customerDetails.setCustomer(customer1);

		Order order1 = new Order(new BigDecimal(10), "OK");
		Order order2 = new Order(new BigDecimal(19), "PARTIAL");

		Employee employee = new Employee("Mietek", "Kowalski");
		order1.setSeller(employee);
		order2.setSeller(employee);
		
		customer1.addOrder(order1).addOrder(order2);
		
		order1.setBuyer(customer1);
		order2.setBuyer(customer1);

		session.save(customer1);
		session.save(order1);
		session.save(order2);

		// komitujemy transakcje
		session.getTransaction().commit();

		// zamykamy sesje
		session.close();

		// czesc druga: tym razem wczytujemy obiekty z bazy, w NOWEJ sesji
		session = sessionFactory.openSession();
		long id = 1;
		session.beginTransaction();
		Customer loadedCustomer = (Customer) session.load(Customer.class, id);
		session.getTransaction().commit();

		for (Order order : loadedCustomer.getOrders()) {
			System.out.println(" order " + order.getId() + " seller: "
					+ order.getSeller().getName() + " buyer: "
					+ order.getBuyer().getName());
		}

		session.close();

	}

}
