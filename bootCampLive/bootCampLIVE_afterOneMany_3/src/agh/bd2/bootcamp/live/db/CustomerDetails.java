package agh.bd2.bootcamp.live.db;

public class CustomerDetails {

	private long id;

	private String address;

	private String phone;

	private int rank;

	// private Customer customer;

	public CustomerDetails(String address, String phone, int rank) {
		super();
		this.address = address;
		this.phone = phone;
		this.rank = rank;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	// public Customer getCustomer() {
	// return customer;
	// }
	//
	// public void setCustomer(Customer customer) {
	// this.customer = customer;
	// }

}
