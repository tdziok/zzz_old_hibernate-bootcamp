package agh.bd2.bootcamp.live.main;

import java.math.BigDecimal;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import agh.bd2.bootcamp.live.db.Customer;
import agh.bd2.bootcamp.live.db.Order;

public class Main {

	public static void main(String[] args) {
		SessionFactory sessionFactory = HibernateUtils.getSessionfactory();
		
		// tworzymy sesjse
		Session session = sessionFactory.openSession();
		// zaczynamy transakcje
		session.beginTransaction();
		
		// zapisujemy obiekty
		
		Customer customer1 = new Customer("HP");
		customer1.addOrder(new Order(new BigDecimal(10), "OK")).addOrder(new Order(new BigDecimal(1000), "PARTIAL"));
		
		
		
		session.save(customer1);
		
		// komitujemy transakcje
		session.getTransaction().commit();
		
		// zamykamy sesje
		session.close();
		
		
		// czesc druga: tym razem wczytujemy obiekty z bazy, w NOWEJ sesji
		session = sessionFactory.openSession();
		long id = 1;
		session.beginTransaction();
		Customer loadedCustomer = (Customer) session.load(Customer.class, id);
		session.getTransaction().commit();

		System.out.println("Loaded: " + loadedCustomer.getName() );
		for( Order order : loadedCustomer.getOrders() )
			System.out.println(" Order " + order.getAmount() + " " + order.getStatus());
		session.close();
		
		 
	}

}
