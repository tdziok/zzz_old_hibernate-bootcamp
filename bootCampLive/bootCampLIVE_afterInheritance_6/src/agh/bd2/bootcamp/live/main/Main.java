package agh.bd2.bootcamp.live.main;

import java.math.BigDecimal;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import agh.bd2.bootcamp.live.db.Customer;
import agh.bd2.bootcamp.live.db.Manager;
import agh.bd2.bootcamp.live.db.Order;
import agh.bd2.bootcamp.live.db.SalesRepresentative;

public class Main {

	public static void main(String[] args) {
		SessionFactory sessionFactory = HibernateUtils.getSessionfactory();

		// tworzymy sesjse
		Session session = sessionFactory.openSession();
		// zaczynamy transakcje
		session.beginTransaction();

		// zapisujemy obiekty
		Manager manager = new Manager("Man_Name", "Man_Surname", "vp");
		SalesRepresentative sal = new SalesRepresentative("SR_Name",
				"SR_Surname", "salesman");

		session.save(manager);
		session.save(sal);

		// komitujemy transakcje
		session.getTransaction().commit();

		// zamykamy sesje
		session.close();

		// czesc druga: tym razem wczytujemy obiekty z bazy, w NOWEJ sesji
		session = sessionFactory.openSession();
		long id = 1;
		session.beginTransaction();
		session.getTransaction().commit();

		session.close();

	}

}
