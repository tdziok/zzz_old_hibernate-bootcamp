package agh.bd2.bootcamp.live.db;

import javax.persistence.Entity;

@Entity
//@DiscriminatorValue("ManagerType")
public class Manager extends Employee {

	private String position;

	public Manager(String name, String surname, String position) {
		super(name, surname);
		this.position = position;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
	
	
	
	
}
