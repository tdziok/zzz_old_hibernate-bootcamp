package agh.bd2.bootcamp.live.db;

import javax.persistence.Entity;

@Entity
//@DiscriminatorValue("SalesRepresentativeType")
public class SalesRepresentative extends Employee {

	private String region;

	public SalesRepresentative(String name, String surname, String region) {
		super(name, surname);
		this.region = region;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

}
