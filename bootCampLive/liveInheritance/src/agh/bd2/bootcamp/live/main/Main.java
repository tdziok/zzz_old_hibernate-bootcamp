package agh.bd2.bootcamp.live.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import agh.bd2.bootcamp.live.db.Customer;
import agh.bd2.bootcamp.live.db.Supplier;

public class Main {

	public static void main(String[] args) {
		SessionFactory sessionFactory = HibernateUtils.getSessionfactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Supplier supplier1 = new Supplier("Supplier1", "krk", ".pl");
		Customer customer1 = new Customer("Customer1", 4, 13);

		session.save(supplier1);
		session.save(customer1);

		transaction.commit();
		session.close();

		session = sessionFactory.openSession();
		transaction = session.beginTransaction();

		long id = 1;
		supplier1 = (Supplier) session.get(Supplier.class, id);
		id = 2;
		customer1 = (Customer) session.get(Customer.class, id);

		transaction.commit();
		session.close();

		System.out.println(supplier1);
		System.out.println(customer1);

	}

}
