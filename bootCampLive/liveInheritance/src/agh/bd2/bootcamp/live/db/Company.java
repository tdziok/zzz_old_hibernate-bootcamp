package agh.bd2.bootcamp.live.db;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
//@DiscriminatorValue("CompanyType")
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(
	    name="companyType",
	    discriminatorType=DiscriminatorType.STRING
	)
public abstract class Company {

	@Id
	@GeneratedValue
	@Column(name="company_id")
	private long id;

	
	protected String name;

	public Company(String name) {
		super();
		this.name = name;
	}
	
	public Company() {
	}

}


