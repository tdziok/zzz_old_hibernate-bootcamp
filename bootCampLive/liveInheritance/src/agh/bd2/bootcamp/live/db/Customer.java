package agh.bd2.bootcamp.live.db;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
//@DiscriminatorValue("CustomerType")
public class Customer extends Company {

	private double discount;
	
	private long succesfulTransactions;

	public Customer(String name, double discount, long succesfulTransactions) {
		super(name);
		this.discount = discount;
		this.succesfulTransactions = succesfulTransactions;
	}
	
	public Customer() {
	}

	@Override
	public String toString() {
		return "Customer [discount=" + discount + ", succesfulTransactions="
				+ succesfulTransactions + ", name=" + name + "]";
	}

	
	
	
}
