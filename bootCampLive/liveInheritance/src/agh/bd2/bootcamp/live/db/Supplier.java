package agh.bd2.bootcamp.live.db;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
// @DiscriminatorValue("SupplierType")
public class Supplier extends Company {

	private String location;

	private String homepage;

	public Supplier(String name, String location, String homepage) {
		super(name);
		this.location = location;
		this.homepage = homepage;
	}

	public Supplier() {
	}

	@Override
	public String toString() {
		return "Supplier [location=" + location + ", homepage=" + homepage
				+ ", name=" + name + "]";
	}


}
