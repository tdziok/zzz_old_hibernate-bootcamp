package agh.bd2.bootcamp.live.main;

import java.math.BigDecimal;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import agh.bd2.bootcamp.live.db.Customer;
import agh.bd2.bootcamp.live.db.CustomerDetails;
import agh.bd2.bootcamp.live.db.Order;

public class Main {

	public static void main(String[] args) {
		SessionFactory sessionFactory = HibernateUtils.getSessionfactory();
		
		// tworzymy sesjse
		Session session = sessionFactory.openSession();
		// zaczynamy transakcje
		session.beginTransaction();
		
		// zapisujemy obiekty
		
		Customer customer1 = new Customer("HP");
		CustomerDetails customerDetails = new CustomerDetails("Krakowska", "111111111", 10);
		
		customer1.setDetails(customerDetails);
		customerDetails.setCustomer(customer1);
		
		session.save(customer1);
		
		// komitujemy transakcje
		session.getTransaction().commit();
		
		// zamykamy sesje
		session.close();
		
		
		// czesc druga: tym razem wczytujemy obiekty z bazy, w NOWEJ sesji
		session = sessionFactory.openSession();
		long id = 1;
		session.beginTransaction();
		Customer loadedCustomer = (Customer) session.load(Customer.class, id);
		session.getTransaction().commit();
		System.out.println("Loaded: " + loadedCustomer.getName() );
		CustomerDetails det = loadedCustomer.getDetails();
		System.out.println("Details: " + det.getCustomer().getName() + " " + det.getRank());
		
		session.close();
		
		 
	}

}
