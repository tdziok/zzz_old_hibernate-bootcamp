package agh.bd2.bootcamp.live.main;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import agh.bd2.bootcamp.live.db.Customer;

public class Main {

	public static void main(String[] args) {
		SessionFactory sessionFactory = HibernateUtils.getSessionfactory();
		
		// tworzymy sesjse
		Session session = sessionFactory.openSession();
		// zaczynamy transakcje
		session.beginTransaction();
		
		// zapisujemy obiekty
		
		Customer customer1 = new Customer("HP");
		session.save(customer1);
		
		// komitujemy transakcje
		session.getTransaction().commit();
		
		// zamykamy sesje
		session.close();
		
		
		// czesc druga: tym razem wczytujemy obiekty z bazy, w NOWEJ sesji
		session = sessionFactory.openSession();
		long id = 1;
		session.beginTransaction();
		Customer loadedCustomer = (Customer) session.load(Customer.class, id);
		session.getTransaction().commit();
		
		Hibernate.initialize(loadedCustomer);
		session.close();
		
		System.out.println("Loaded: " + loadedCustomer.getName() );
		 
	}

}
