package agh.bd2.bootcamp.live.db;

import javax.persistence.Entity;
import javax.persistence.Id;

//@Entity
public class Customer {

	//@Id
	private long id;

	private String name;

	// private CustomerDetails details;
	//
	// private List<Order> orders;

	public Customer(String name) {
		super();
		this.name = name;
	}
	Customer() {
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
//
//	 public CustomerDetails getDetails() {
//	 return details;
//	 }
//	
	// public void setDetails(CustomerDetails details) {
	// this.details = details;
	// }
	//
	// public List<Order> getOrders() {
	// return orders;
	// }
	//
	// public void setOrders(List<Order> orders) {
	// this.orders = orders;
	// }

}
