package agh.bd2.bootcamp.live.db;

import java.math.BigDecimal;

public class Order {
	private long id;
	private BigDecimal amount;
	private String status;
//	private Employee seller;
//	private Customer buyer;
//	
	Order() {
	}

	public Order(BigDecimal amount, String status) {
		super();
		this.amount = amount;
		this.status = status;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public String getStatus() {
		return status;
	}
	public long getId() {
		return id;
	}
//	public Customer getBuyer() {
//		return buyer;
//	}
//	public Employee getSeller() {
//		return seller;
//	}
//
//	public void setSeller(Employee seller) {
//		this.seller = seller;
//	}
//
//	public void setBuyer(Customer buyer) {
//		this.buyer = buyer;
//	}
	
	public void setId(long id) {
		this.id = id;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
}
