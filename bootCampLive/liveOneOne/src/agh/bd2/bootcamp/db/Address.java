package agh.bd2.bootcamp.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ADDRESSES")
public class Address {
	@Id
	@GeneratedValue
	private long id;
	@Column(length=255)
	private String city;
	
	private int houseNr;

	
	public Address(String city, int houseNr) {
		super();
		this.city = city;
		this.houseNr = houseNr;
	}

	Address() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getHouseNr() {
		return houseNr;
	}

	public void setHouseNr(int houseNr) {
		this.houseNr = houseNr;
	}
	
	
}
