package agh.bd2.bootcamp.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import agh.bd2.bootcamp.db.Person;


public class Main {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Wczytujemy konfigurację hibernate
		SessionFactory sessionFactory = new Configuration().configure()
				.buildSessionFactory();
		// otwieramy sesję do bazy
		Session session = sessionFactory.openSession();

		Person person = new Person("Wiesiek", "Nowak");

		session.beginTransaction();
		session.save(person);
		// zmieniamy coś
		person.setName("Jozek");
		
		session.getTransaction().commit();
		
		Person person2 = new Person("Tomek", "Warszawski");
		// session.beginTransaction();
		session.save(person2);
		
		person2.setSurname("Krakowski");
		
		session.saveOrUpdate(person2);
		
		session.flush();
		// session.getTransaction().commit();

		session.close();
		
		
		//Wczytajmy coś 
		session = sessionFactory.openSession();
		//session.beginTransaction();
		Person loadedPerson = (Person) session.load(Person.class, 1L);
		//session.getTransaction().commit();
		System.out.println("Wczytano : " + loadedPerson.getName() + " " + loadedPerson.getSurname());
		session.close();

	}

}
