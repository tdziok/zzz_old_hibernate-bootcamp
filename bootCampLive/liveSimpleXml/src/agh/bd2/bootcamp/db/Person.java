package agh.bd2.bootcamp.db;

public class Person {
	private long id;
	private String name;
	private String surname;

	// obowiązkowy konstruktor bezparametrowy
	Person() {}

	public Person(String name, String surname) {
		super();
		this.name = name;
		this.surname = surname;
	}
	
	public long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getSurname() {
		return surname;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	
}
