package agh.bd2.bootcamp.db;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Type;

@Entity
//@Table(name="PEOPLE") 
public class Person {
	@Id
	@GeneratedValue
	@Column(name="PERSON_ID")
	private long id;
	
	@Column(length=255)
	private String name;
	@Column(length=255)
	@Type(type="string")
	private String surname;

	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY/*,mappedBy="person"*/ )
	@JoinColumn(name= "PERSON_ID")
	private List<Address> addresses = new ArrayList<Address>();
	
	
	// obowiązkowy konstruktor bezparametrowy
	Person() {}

	public Person(String name, String surname) {
		super();
		this.name = name;
		this.surname = surname;
	}
	public Person addAddress(Address addr){
//		/addr.setPerson(this);
		this.addresses.add(addr);
		return this;
	}
	
	public long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getSurname() {
		return surname;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	
}
