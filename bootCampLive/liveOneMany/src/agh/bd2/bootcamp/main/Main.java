package agh.bd2.bootcamp.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import agh.bd2.bootcamp.db.Address;
import agh.bd2.bootcamp.db.Person;

public class Main {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Wczytujemy konfigurację hibernate
		SessionFactory sessionFactory = new Configuration().configure()
				.buildSessionFactory();
		// otwieramy sesję do bazy
		Session session = sessionFactory.openSession();

		session.beginTransaction();

		Person person = new Person("Tom", "Hanks");
		person.addAddress(new Address("Krakow", 100))
			.addAddress(new Address("New York", 99))
			.addAddress(new Address("Warszawa", 199));

		session.save(person);
		session.getTransaction().commit();

		session.close();

	}

}
